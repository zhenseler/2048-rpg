﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AttackImage : MonoBehaviour
{
    [SerializeField] Color fiveTurnColor;
    [SerializeField] Color nextTurnColor;
    [SerializeField] float pulseSpeed = 1f;
    

    Color defaultColor;
    Color pulseStartColor;
    Color pulseEndColor;
    Image image;
    bool pulse = false;
    float i = 0;

    private void Start() 
    {
        image = GetComponent<Image>();
        defaultColor = image.color;
        pulseStartColor = fiveTurnColor;
        pulseEndColor = nextTurnColor;
    }

    private void Update() 
    {
        if(pulse) {PulseImage();}
    }

    public void UpdateColor(int countdown)
    {
        if(countdown == 1) {pulse = true; return;}
        if(countdown <= 5) {pulse = false; image.color = fiveTurnColor; return;}
        image.color = defaultColor;
        pulse = false;

    }

    private void PulseImage()
    {
        i += Time.deltaTime * pulseSpeed;
        image.color = Color.Lerp(pulseStartColor, pulseEndColor, Mathf.PingPong(i * 2, 1)); 
        if(i >= 1) 
        {
            i = 0;
            pulseStartColor = image.color;
            pulseEndColor = fiveTurnColor;
        }
        if(i <= 0)
        {
            pulseStartColor = image.color;
            pulseEndColor = nextTurnColor;
        }
    }
}
