﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Defense : MonoBehaviour
{
    // Config params
    [SerializeField] TextMeshProUGUI chargeText;
    [SerializeField] List<Slider> shieldSliderList = new List<Slider>();
    [SerializeField] Collector collector;
    [SerializeField] int startingCharge = 10;
    [SerializeField] int maxChargePerShield = 100;
    [SerializeField] float cooldown = 2f;
    [SerializeField] Color shieldUnchargedColor;
    [SerializeField] Color shieldChargedColor;

    // State variables
    int totalCharge;
    int shieldCount; // # of shields on ship
    int maxCharge; // Max charge of shield based off number of shields
    Slider chargingShield; // The shield that is currently charging (unfilled)
    int chargingShieldIndex;
    int chargeOnCurrentShield; // The charge on the first unfilled shield
    int tileLayerMask = 1 << 8;
    float timeLastCollected = -1;
    Color defaultSliderFillColor;


    void Start()
    {
        totalCharge = startingCharge;
        shieldCount = shieldSliderList.Count;
        maxCharge = shieldCount * maxChargePerShield;
        DetermineCurrentChargingShield();
        UpdateText();
        UpdateSlider();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.E))
        {
            AddShield(collector.CollectBlock());
            //UpdateShields();
        }
    }

    private void DetermineCurrentChargingShield()
    {
        print("Determine run");
        int currentChargeDividedByChargePerShield = (int)Mathf.Floor(totalCharge / maxChargePerShield);
        chargingShieldIndex = Mathf.Clamp(currentChargeDividedByChargePerShield, 0, shieldCount - 1);
        print(chargingShieldIndex);
        chargingShield = shieldSliderList[chargingShieldIndex];
        print(chargingShield.name);
        chargeOnCurrentShield = totalCharge - (chargingShieldIndex * maxChargePerShield);
        print(chargeOnCurrentShield);
    }


    public void AddShield(int amount)
    {
        totalCharge = Mathf.Clamp(totalCharge + amount, 0, maxCharge);
        UpdateShields();
    }

    private void UpdateShields()
    {
        DetermineCurrentChargingShield();
        UpdateSlider();
        UpdateText();
    }

    public void TakeDamage(int amount, GameObject source)
    {
        if(totalCharge < maxChargePerShield)
        {
            MonsterAI monsterAI = source.GetComponent<MonsterAI>(); // In case I want to add damage from other sources
            if(monsterAI)
            {
                monsterAI.UnblockedHit();
            }
        }
        totalCharge = Mathf.Clamp(totalCharge - amount, 0, 100);
        UpdateShields();
    }

    public void UpdateText()
    {
        if(chargingShieldIndex == shieldCount - 1 && chargingShield.value == 1)
        {
            chargeText.text = "MAX";
        }
        else
        {
            chargeText.text = (maxChargePerShield - chargeOnCurrentShield).ToString();

        }
    }

    private void UpdateSlider()
    {
        for(int i = 0; i < chargingShieldIndex; i++) // color newly charged shields
        {
            print("LOOP RUN NUMBER " + i.ToString());
            Slider updatingSlider = shieldSliderList[i];
            updatingSlider.value = 1;
            updatingSlider.transform.Find("Fill Area").transform.Find("Shield Fill").transform.GetComponent<Image>().color = shieldChargedColor;
        }
        for(int i = chargingShieldIndex + 1; i < shieldCount; i++) // color newly uncharged shields
        {
            print(i);
            shieldSliderList[i].value = 0;
            Image fill = chargingShield.transform.Find("Fill Area").transform.Find("Shield Fill").transform.GetComponent<Image>();
            fill.color = shieldUnchargedColor;
        }
        chargingShield.value = chargeOnCurrentShield / (float)maxChargePerShield;
        Image shieldFill = chargingShield.transform.Find("Fill Area").transform.Find("Shield Fill").transform.GetComponent<Image>();
        if(chargingShield.value == 1)
        {
            shieldFill.color = shieldChargedColor;
        }
        else
        {
            shieldFill.color = shieldUnchargedColor;
        }
    }


    public float GetCooldown()
    {
        return cooldown;
    }
}
