﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cannon : MonoBehaviour
{

    // Config params
    [SerializeField] Collector collector;
    [SerializeField] float cooldown = 2f;
    [SerializeField] string collectionKey;

    // State vars
    int monsterLayerMask = 1 << 10;
    KeyCode keycode;


    void Start()
    {
        keycode = (KeyCode)System.Enum.Parse(typeof(KeyCode), collectionKey) ;
    }

    void Update()
    {
        if(Input.GetKeyDown(keycode))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, Vector3.up, out hit, Mathf.Infinity, monsterLayerMask))
        {
            MonsterAI monsterAI = hit.transform.GetComponent<MonsterAI>();
            if(monsterAI)
            {
                int damage = collector.CollectBlock();
                monsterAI.DecreaseHealth(damage);
            }
            else
            {
                print("Hit something that is not the attacker");
            }
        }

        // calculate damage from block value
        // grab monster
        // do damage to monster equal to that
    }
}
