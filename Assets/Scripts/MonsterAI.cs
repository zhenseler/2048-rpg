﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MonsterAI : MonoBehaviour
{
    // Config params
    [SerializeField] int monsterHealth = 500;
    [SerializeField] int minAttack = 1;
    [SerializeField] int maxAttack = 2;
    [SerializeField] float minAttackInterval = 10;
    [SerializeField] float maxAttackInterval = 15;
    [SerializeField] float firstAttackDelay; // delay first attack will have
    [SerializeField] Image attackCooldownImage;
    


    // State vars
    float timeSinceLastAttack;
    float attackTime; // Time at which next attack should occur
    float attackCooldown;
    int attackDamage;
    bool firstattack = true;
    TileSpawner tileSpawner;
    bool realtime;




    // Cached refs
    Defense playerDefense;


    void Start()
    {
        playerDefense = FindObjectOfType<Defense>();
        attackCooldown = maxAttackInterval + firstAttackDelay;
        attackDamage = CalculateAttackDamage();
        attackTime = maxAttackInterval;
        tileSpawner = FindObjectOfType<TileSpawner>();
        realtime = tileSpawner.GetGamePlayMode();
    }

    void Update()
    {
        TrackRealtimeAttack();
    }

    private void TrackRealtimeAttack()
    {
        if(attackCooldown >= 0)
        {
            attackCooldown -= Time.deltaTime;
            // Should possibly only have timer show up slightly before attack
            if(attackCooldown <= maxAttackInterval) // Will not display until the first attack
            {
                float fillAmount = 1 - (attackCooldown / attackTime);
                attackCooldownImage.fillAmount = fillAmount;
            }
            else
            {
                attackCooldownImage.fillAmount = 0;
            }
        }
        else
        {
            Attack();
        }
    }


    private void CalculateNextAttack()
    {
        attackCooldown = Random.Range(minAttackInterval, maxAttackInterval);
        attackTime = attackCooldown;
        attackDamage = CalculateAttackDamage();
    }

    private int CalculateAttackDamage()
    {
        int damage = Random.Range(minAttack, maxAttack);
        return damage;
    }

    private void Attack()
    {
        playerDefense.TakeDamage(attackDamage, gameObject);
        CalculateNextAttack();
    }

    public void UnblockedHit()
    {
        MonsterTileSpawner monsterTileSpawner = GetComponent<MonsterTileSpawner>();
        if(monsterTileSpawner)
        {
            if(tileSpawner.Maxed())
            {
                tileSpawner.EndGame();
            }
            monsterTileSpawner.SpawnTiles();
        }
    }

    public void DecreaseHealth(int amount)
    {
        monsterHealth = Mathf.Clamp(monsterHealth - amount, 0, monsterHealth);
        if(monsterHealth == 0)
        {
            Die();
        }
    }

    private void Die()
    {
        Destroy(gameObject);
    }
}
