﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Collector : MonoBehaviour
{

    // Config params
    [SerializeField] float cooldown;
    [SerializeField] Image cooldownImage;


    // State vars
    int tileLayerMask = 1 << 8;
    float timeLastCollected = -1;
    float cooldownTimeLeft = 0;



    void Update()
    {
        TrackCooldown();
    }

    public int CollectBlock()
    {
        if(OffCooldown())
        {
            RaycastHit hit;
            if(Physics.Raycast(transform.position, Vector3.forward, out hit, Mathf.Infinity, tileLayerMask))
            {
                NumberTile hitTile = hit.transform.GetComponentInParent<NumberTile>();
                if(hitTile)
                {
                    int tileValue = hitTile.GetValue();
                    hitTile.RemoveTile();
                    cooldownTimeLeft = cooldown;
                    return tileValue;
                }
                cooldownTimeLeft = cooldown;
            }
        }
        return 0; // in null case, return 0
    }

    private bool OffCooldown()
    {
        if(cooldownTimeLeft <= 0) {return true;}
        else {return false;}
    }

    private void TrackCooldown()
    {
        if(cooldownTimeLeft >= 0)
        {
            cooldownTimeLeft -= Time.deltaTime;
            cooldownImage.fillAmount = Mathf.Clamp(cooldownTimeLeft / cooldown, 0, 1);
        }
    }


}
