﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundScroller : MonoBehaviour
{

    // Config params
    [SerializeField] float speed;

    // Cached refs
    Material material;
    Vector2 offset;

    private void Start() 
    {
        material = GetComponent<Renderer>().material;
        offset = new Vector2(0, speed);
    }

    void Update()
    {
        material.mainTextureOffset += offset * Time.deltaTime;
    }
}
