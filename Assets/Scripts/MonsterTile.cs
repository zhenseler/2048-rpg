﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterTile : MonoBehaviour
{
    [SerializeField] bool canEnterNonEjectCollectors = false;

    public bool CanEnterNonEjectCollectors()
    {
        if(canEnterNonEjectCollectors) {return true;}
        else {return false;}
    }
}
