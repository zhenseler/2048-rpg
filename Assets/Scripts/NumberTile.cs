﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NumberTile : MonoBehaviour
{
    // Config params
    [SerializeField] TextMesh tileText;
    [SerializeField] int tileValue = 2;
    [SerializeField] float tileSpeed;
    [SerializeField] GameObject boxColliderObject;
    [SerializeField] bool unmergeable = false;
    [SerializeField] AudioClip mergeClip;
    
    // State vars
    int xmin, xmax, ymin, ymax;
    bool posLocked = false;
    bool mergeLocked = false;
    int tileLayerMask = 1 << 8;
    int boardLayerMask = 1 << 9;
    int collectorLayerMask = 1 << 11;
    Vector3 destination;
    Vector3 nextDestination;
    bool merging = false; // If tile is going to merge into another tile or not
    NumberTile mergingTile = null; // Stores tile that is about to be merged with
    bool newDestinationFound = false; // If searching for a new destination is finished or not
    bool isMoving = false; // Set to true as long as tile is not at destination



    // Cached refs
    TileSpawner tileSpawner;
    Animator animator;
    AudioSource audioSource;


    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        tileSpawner = FindObjectOfType<TileSpawner>();
        animator = GetComponent<Animator>();

        if(tileValue > 0) {tileText.text = tileValue.ToString();}
        else {tileText.text = "";}
        gameObject.name = transform.position.y.ToString();
        destination = transform.position;
        UpdateColor();
    }

    private void Update() 
    {
        if(transform.position != destination)
        {
            MoveToPosition();
            isMoving = true;
        }
        else // if tile is at destination
        {
            if(merging) // if it's about to merge, up the other tile and destroy this one
            {
                mergingTile.SetValue(tileValue * 2);
                mergingTile.mergeLocked = true;
                AudioSource.PlayClipAtPoint(mergeClip, transform.position);
                mergingTile.UpdateColor();

                RemoveTile();
            }

            destination = transform.position; // This doesn't need to be set every frame
            mergeLocked = false;  // Does this need to be here? Can collapse all into one function
            if(isMoving) // if the tile had been put into motion
            {
                isMoving = false;
                tileSpawner.movingTileCount--; // Since tile has reached destination, remove it from the count
            }
        }
    }


    // Tile Movement

    private void MoveToPosition()
    {
        Vector3 currentPosition = transform.position;
        Vector3 nextPosition = Vector3.MoveTowards(currentPosition, destination, Time.deltaTime * tileSpeed);
        transform.position = nextPosition;
    }

    public void CalculateNewDestination(Vector3 direction)
    {
        RaycastHit hit;
        newDestinationFound = false;
        nextDestination = transform.position;
        while(!newDestinationFound)
        {
            Vector3 nextVector = nextDestination + direction;

            // if there is a tile or the board
            if(Physics.Raycast((nextVector + new Vector3(0,0,-20)), Vector3.forward, out hit, Mathf.Infinity, tileLayerMask | boardLayerMask | collectorLayerMask))
            {
                NumberTile otherTile = hit.transform.GetComponentInParent<NumberTile>();

                // if hit a mergeable tile
                if(otherTile && otherTile.GetValue() == tileValue && !otherTile.mergeLocked && !otherTile.mergingTile && !unmergeable && !otherTile.unmergeable)
                {
                    ProcessMerge(otherTile);
                    return;
                }

                else if(otherTile | hit.transform.gameObject.layer == 11) // if hit a non-mergable tile
                {
                    SetTarget();
                }

                else if(hit.transform.tag == "Board")
                {
                    nextDestination = nextVector;
                }
            }

            else // Will trigger from unready 
            {
                SetTarget();
            }
        }
    }

    private void SetTarget()
    {
        newDestinationFound = true;
        if(transform.position != nextDestination) // If the tile needs to move
        {
            boxColliderObject.transform.position = nextDestination;
            FlickerBoxCollider();
            tileSpawner.movingTileCount++;
            tileSpawner.spawnTile = true;
        }
    }

    private void ProcessMerge(NumberTile otherTile)
    {
        merging = true;
        mergingTile = otherTile;
        otherTile.mergeLocked = true;
        newDestinationFound = true;
        nextDestination = otherTile.GetNextDestination();
        tileSpawner.movingTileCount++;
        tileSpawner.spawnTile = true;
        //otherTile.UpdateColor();
        boxColliderObject.GetComponent<BoxCollider>().enabled = false;
    }

    private void FlickerBoxCollider()
    {
        boxColliderObject.GetComponent<BoxCollider>().enabled = false;
        boxColliderObject.GetComponent<BoxCollider>().enabled = true;
    }

    public void ResetTile()
    {
        posLocked = false;
        mergeLocked = false;
    }

    public void RemoveTile()
    {
        tileSpawner.RemoveTileFromList(this);
        tileSpawner.onMovementStart -= RemoveTile;
        Destroy(gameObject);
    }

    private void UpdateColor()
    {
        if(unmergeable) {return;}
        float newColorHue = (12 * (Mathf.Log(tileValue) / Mathf.Log(2)))/360;
        Color newColor = Color.HSVToRGB(newColorHue, 1, 1);
        GetComponentInChildren<SpriteRenderer>().color = newColor;
    }


    // Getters & Setters

    public void SetNewDestination()
    {
        boxColliderObject.transform.position = transform.position;
        destination = nextDestination;
    }

    public Vector3 GetNextDestination()
    {
        return nextDestination;
    }

    public int GetValue()
    {
        return tileValue;
    }

    public void SetValue(int value)
    {
        tileValue = value;
        tileText.text = tileValue.ToString();
    }

    public void SetBounds(int xMin, int xMax, int yMin, int yMax)
    {
        xmin = xMin;
        xmax = xMax;
        ymin = yMin;
        ymax = yMax;
    }

    public int GetXPosition()
    {
        return Mathf.RoundToInt(transform.position.x);
    }

    public int GetYPosition()
    {
        return Mathf.RoundToInt(transform.position.y);
    }

    public bool GetLockedStatus()
    {
        return posLocked;
    }

    public bool GetMergedStatus()
    {
        return mergeLocked;
    }
}
