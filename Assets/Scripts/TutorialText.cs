﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TutorialText : MonoBehaviour
{
    List<Transform> childList = new List<Transform>();
    int counter;

    private void Start() 
    {
        foreach(Transform child in transform)
        {
            childList.Add(child);
        }

    }

    private void Update() 
    {
        if(Input.anyKeyDown)
        {
            counter++;
            if(counter < childList.Count)
            {
                childList[counter - 1].gameObject.SetActive(false);
                childList[counter].gameObject.SetActive(true);
            }
            if(counter == childList.Count)
            {
                SceneManager.LoadScene(2);
            }
        }
    }
}
