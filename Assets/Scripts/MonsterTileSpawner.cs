﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterTileSpawner : MonoBehaviour
{

    // Config params
    [SerializeField] NumberTile monsterTile;
    [SerializeField] int numberToSpawn = 1;
    
    // Cached refs
    TileSpawner tileSpawner;

    private void Start() 
    {
        tileSpawner = FindObjectOfType<TileSpawner>();
    }

    public void SpawnTiles()
    {
        for(int i = 0; i < numberToSpawn; i++)
        {
            tileSpawner.SpawnNewTile(monsterTile);
        }
    }
}
