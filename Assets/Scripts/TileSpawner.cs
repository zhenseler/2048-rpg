﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class TileSpawner : MonoBehaviour
{

    // Config params
    [SerializeField] int xmin, xmax, ymin, ymax;
    [SerializeField] int totalValidSpots;
    [SerializeField] List<NumberTile> numberTilePrefabList = new List<NumberTile>();
    [SerializeField] List<float> tileProbabilities = new List<float>();
    [SerializeField] NumberTile testSpawnTile;
    [SerializeField] Canvas gameOverCanvas;
    [SerializeField] bool realtime;

    // State variables
    List<NumberTile> numberTileList = new List<NumberTile>();
    public event System.Action onMovementCycleEnd;
    public event System.Action onMovementStart;
    int tileMask = 1 << 8;
    int boardMask = 1 << 9;
    int collectorMask = 1 << 11;
    List<NumberTile> toDelete = new List<NumberTile>();
    public int movingTileCount = 0;
    public bool spawnTile = true;
    bool allowMovement = true;

    KeyCode upKey;
    KeyCode downKey;
    KeyCode leftKey;
    KeyCode rightKey;
    KeyBindings keyBindings;


    public event System.Action onTurnFinish;



    void Start()
    {
        //Debug.DrawRay(new Vector3(2.5f, 4.5f, -1), Vector3.forward * 30, Color.white, 120f);
        if(numberTilePrefabList.Count != tileProbabilities.Count)
        {
            throw new System.Exception("Length of numberTilePrefabs and tileProbabilities unequal");
        }
        if(tileProbabilities.Sum() != 1)
        {
            throw new System.Exception("Tile spawning probabilities do not add up to 1");
        }
        keyBindings = FindObjectOfType<KeyBindings>();
        upKey = keyBindings.upMovementKey;
        downKey = keyBindings.downMovementKey;
        leftKey = keyBindings.leftMovementKey;
        rightKey = keyBindings.rightMovementKey;
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.T))
        {
            SpawnNewTile(DetermineTileToSpawn());
        }
        if(Input.GetKeyDown(KeyCode.U))
        {
            SpawnNewTile(testSpawnTile);
        }
        ProcessTileMovement();
        if(numberTileList.Count < totalValidSpots)
        {
            SpawnTileIfNeeded();
        }
    }

    private void SpawnTileIfNeeded()
    {
        if(movingTileCount == 0 && spawnTile == true)
        {
            SpawnNewTile(DetermineTileToSpawn());
            spawnTile = false;
            allowMovement = true;
            if(!realtime)
            {
                onTurnFinish(); // When in turn-based play, runs all methods that should run after one successful movement
            }
        }
    }

    private NumberTile DetermineTileToSpawn()
    {
        float probabilityTracker = 0;
        int tileIndex = 0;
        float randomProb = Random.Range(0.0f,1.0f);
        foreach(float probability in tileProbabilities)
        {
            probabilityTracker += probability;
            if(randomProb <= probabilityTracker)
            {
                return numberTilePrefabList[tileIndex];
            }
            else
            {
                tileIndex++;
            }
        }
        return numberTilePrefabList[tileIndex];
    }

    public void SpawnNewTile(NumberTile numberTile)
    {
        int xPos;
        int yPos;
        while(true)
        {
            GenerateRandomCoords(out xPos, out yPos);
            if(CheckCoords(xPos, yPos))
            {
                NumberTile newTile = Instantiate(numberTile, new Vector3(xPos, yPos, 0), transform.rotation) as NumberTile;
                newTile.SetBounds(xmin, xmax, ymin, ymax);
                numberTileList.Add(newTile);
                print(numberTileList.Count);
                print(totalValidSpots);
                CheckLoseCondition();
                return;
            }
        }
    }

    private void GenerateRandomCoords(out int xPos, out int yPos)
    {
        xPos = Random.Range(xmin, xmax);
        yPos = Random.Range(ymin, ymax);
    }

    private bool CheckCoords(float xPos, float yPos)
    {
        RaycastHit hit;
        if(Physics.Raycast(new Vector3(xPos, yPos, -20), Vector3.forward, out hit, Mathf.Infinity, tileMask | collectorMask | boardMask))
        {
            if(hit.transform.tag == "Board")
            {
                return true;
            }
        }
        return false;
    }

    private void ProcessTileMovement()
    {
        if(movingTileCount == 0) {allowMovement = true;}
        string direction= null;
        if(Input.GetKeyDown(upKey))
        {
            direction = "up";
        }
        else if(Input.GetKeyDown(leftKey))
        {
            direction = "left";
        }
        else if(Input.GetKeyDown(downKey))
        {
            direction = "down";
        }
        else if(Input.GetKeyDown(rightKey))
        {
            direction = "right";
        }
        if(direction != null && allowMovement)
        {
            allowMovement = false;
            MoveTiles(direction);
        }
    }

    // Should probably be an enum instead of a string
    private void MoveTiles(string direction)
    {
        if(direction == "up"){MoveUp();}
        else if(direction == "down"){MoveDown();}
        else if(direction == "left"){MoveLeft();}
        else if(direction == "right"){MoveRight();}
        foreach (NumberTile numberTile in numberTileList)
        {
            numberTile.ResetTile();
        }
    }

    public void RemoveTileFromList(NumberTile numberTile)
    {
        numberTileList.Remove(numberTile);
    }

    private bool CheckForActiveTiles()
    {
        foreach(NumberTile numberTile in numberTileList)
        {
            if(numberTile.GetLockedStatus() == false)
            {
                return true;
            }
        }
        return false;
    }

    private void MoveDown()
    {
        for (int i = ymin; i < ymax; i++)
        {
            foreach(NumberTile numberTile in numberTileList)
            {
                if(numberTile.GetYPosition() == i)
                {
                    numberTile.CalculateNewDestination(Vector3.down);
                }
            }
        }
        if(onMovementStart != null) {onMovementStart();}
        foreach(NumberTile numberTile in numberTileList)
        {
            numberTile.SetNewDestination();
        }
    }

    private void MoveUp()
    {
        for (int i = ymax - 1; i > ymin - 1; i--)
        {
            foreach(NumberTile numberTile in numberTileList)
            {
                if(numberTile.GetYPosition() == i)
                {
                    numberTile.CalculateNewDestination(Vector3.up);
                }
            }
        }
        if(onMovementStart != null) {onMovementStart();}
        foreach(NumberTile numberTile in numberTileList)
        {
            numberTile.SetNewDestination();
        }
    }

    private void MoveLeft()
    {
        for (int i = xmin; i < xmax; i++)
        {
            foreach(NumberTile numberTile in numberTileList)
            {
                if(numberTile.GetXPosition() == i)
                {
                    numberTile.CalculateNewDestination(Vector3.left);
                }
            }
        }
        if(onMovementStart != null) {onMovementStart();}
        foreach(NumberTile numberTile in numberTileList)
        {
            numberTile.SetNewDestination();
        }
    }

    private void MoveRight()
    {
        for (int i = xmax - 1; i > xmin - 1; i--)
        {
            foreach(NumberTile numberTile in numberTileList)
            {
                if(numberTile.GetXPosition() == i)
                {
                    numberTile.CalculateNewDestination(Vector3.right);
                }
            }
        }
        if(onMovementStart != null) {onMovementStart();}
        foreach(NumberTile numberTile in numberTileList)
        {
            numberTile.SetNewDestination();
        }
    }

    public bool Maxed()
    {
        if(numberTileList.Count == totalValidSpots) {print("MAXED"); return true;}
        else {return false;}
    }

    public void EndGame()
    {
        gameOverCanvas.enabled = true;
        print("Game over");
        //Time.timeScale = 0;
    }

    public bool GetGamePlayMode()
    {
        return realtime;
    }

    public void DecreaseAvailableSpaces(int amount)
    {
        totalValidSpots -= amount;
    }

    public void IncreaseAvailableSpaces(int amount)
    {
        totalValidSpots += amount;
    }

    private void CheckLoseCondition()
    {
        if(numberTileList.Count == totalValidSpots)
        {
            EndGame();
        }
    }
}
