﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyBindings : MonoBehaviour
{
    [Header("Block Movement")]
    public KeyCode upMovementKey;
    public KeyCode downMovementKey;
    public KeyCode leftMovementKey;
    public KeyCode rightMovementKey;

    [Header("Ship Controls")]
    public KeyCode shieldKey;
    public KeyCode cannonKey;
    public KeyCode ejectorKey;

    private void Start() 
    {
    }

}
