﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TbMonsterAI : MonoBehaviour
{
    // Config params
    [SerializeField] int monsterHealth = 500;
    [SerializeField] int minAttackInShieldUnits = 1;
    [SerializeField] int maxAttackInShieldUnits = 2;
    [SerializeField] int minAttackInterval = 10;
    [SerializeField] int maxAttackInterval = 15;
    [SerializeField] int firstAttackDelay; // delay first attack will have
    [SerializeField] AudioClip deathClip;
    [SerializeField] ParticleSystem hitFX;
    [SerializeField] AudioClip hitClip;

    [Header("UI")]
    [SerializeField] Image attackImage;
    [SerializeField] TextMeshProUGUI countdownText;
    [SerializeField] TextMeshProUGUI healthText;
    [SerializeField] Canvas winCanvas; // Shouldn't really be on the monster. Move later
    


    // State vars
    float timeSinceLastAttack;
    int attackCountdown;
    int attackDamage;
    bool firstattack = true;
    TileSpawner tileSpawner;
    int maxChargePerShield;
    AttackImage attackImageComponent;
    AudioSource audioSource;


    // Cached refs
    TbShields shields;


    void Start()
    {
        shields = FindObjectOfType<TbShields>();
        attackImageComponent = attackImage.transform.GetComponent<AttackImage>();
        audioSource = GetComponent<AudioSource>();

        attackCountdown = maxAttackInterval + firstAttackDelay;
        attackDamage = CalculateAttackDamage();
        tileSpawner = FindObjectOfType<TileSpawner>();
        UpdateAttackCountdownText();
        UpdateHealthText();
        tileSpawner.onTurnFinish += TrackTbAttack;
        maxChargePerShield = shields.GetMaxChargePerShield();
    }


    private void TrackTbAttack()
    {
        attackCountdown--;
        if(attackCountdown <= 0)
        {
            Attack();
            CalculateNextAttack();
        }
        UpdateAttackCountdownText();
    }


    private void CalculateNextAttack()
    {
        attackCountdown = Random.Range(minAttackInterval, maxAttackInterval);
        attackDamage = CalculateAttackDamage();
    }


    private int CalculateAttackDamage()
    {
        int damage = Random.Range(minAttackInShieldUnits, maxAttackInShieldUnits);
        return  maxChargePerShield*  damage;
    }


    private void Attack()
    {
        shields.TakeDamage(attackDamage, gameObject);
        CalculateNextAttack();
    }

    public void UnblockedHit()
    {
        MonsterTileSpawner monsterTileSpawner = GetComponent<MonsterTileSpawner>();
        if(monsterTileSpawner)
        {
            if(tileSpawner.Maxed())
            {
                tileSpawner.EndGame();
            }
            monsterTileSpawner.SpawnTiles();
        }
    }

    private void UpdateAttackCountdownText()
    {
        countdownText.text = attackCountdown.ToString();
        attackImageComponent.UpdateColor(attackCountdown);
        //attackImageComponent.UpdateColor(attackCountdown);
        // Add flashing/pulsing if it is about to attack
    }

    public void DecreaseHealth(int amount)
    {
        audioSource.PlayOneShot(hitClip);
        ParticleSystem VFX = Instantiate(hitFX, transform.position, transform.rotation) as ParticleSystem;
        Destroy(VFX.gameObject, 2);
        monsterHealth = Mathf.Clamp(monsterHealth - amount, 0, monsterHealth);
        UpdateHealthText();
        if(monsterHealth == 0)
        {
            Die();
        }
    }

    private void Die()
    {
        tileSpawner.onTurnFinish -= TrackTbAttack;
        AudioSource.PlayClipAtPoint(deathClip, transform.position);
        winCanvas.enabled = true;
        Destroy(gameObject);
    }

    private void UpdateHealthText()
    {
        healthText.text = monsterHealth.ToString();
    }
}

