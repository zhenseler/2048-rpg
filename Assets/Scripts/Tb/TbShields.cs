﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TbShields : MonoBehaviour
{
    // Config params
    [SerializeField] TextMeshProUGUI chargeText;
    [SerializeField] List<Slider> shieldSliderList = new List<Slider>();
    [SerializeField] List<TbCollector> collectorList = new List<TbCollector>();
    [SerializeField] int startingCharge = 10;
    [SerializeField] int maxChargePerShield = 100;
    [SerializeField] Color shieldUnchargedColor;
    [SerializeField] Color shieldChargedColor;
    [SerializeField] AudioClip hitOnChargedShieldClip;
    [SerializeField] AudioClip unblockedAttackClip;
    [SerializeField] AudioClip chargeClip;

    // State variables
    int totalCharge;
    int shieldCount; // # of shields on ship
    int maxCharge; // Max charge of shield based off number of shields
    Slider chargingShield; // The shield that is currently charging (unfilled)
    int chargingShieldIndex;
    int chargeOnCurrentShield; // The charge on the first unfilled shield
    int tileLayerMask = 1 << 8;
    Color defaultSliderFillColor;
    KeyCode keycode;

    // Cached refs
    AudioSource audioSource;


    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        totalCharge = startingCharge;
        shieldCount = shieldSliderList.Count;
        maxCharge = shieldCount * maxChargePerShield;
        keycode = FindObjectOfType<KeyBindings>().shieldKey;
        UpdateShields();
    }

    void Update()
    {
        if(Input.GetKeyDown(keycode))
        {
            foreach (TbCollector collector in collectorList)
            {
                AddShield(collector.CollectBlock());
                collector.ResetCountdown();
            }
        }
    }

    private void DetermineCurrentChargingShield()
    {
        int currentChargeDividedByChargePerShield = (int)Mathf.Floor(totalCharge / maxChargePerShield);
        chargingShieldIndex = Mathf.Clamp(currentChargeDividedByChargePerShield, 0, shieldCount - 1);
        chargingShield = shieldSliderList[chargingShieldIndex];
        chargeOnCurrentShield = totalCharge - (chargingShieldIndex * maxChargePerShield);
    }


    public void AddShield(int amount)
    {
        if(amount < 0) // When collecting a negative monster block
        {
            TakeDamage(maxChargePerShield, gameObject);
            return;
        }
        audioSource.PlayOneShot(chargeClip);
        totalCharge = Mathf.Clamp(totalCharge + amount, 0, maxCharge);
        UpdateShields();
    }

    private void UpdateShields()
    {
        DetermineCurrentChargingShield();
        UpdateSlider();
        UpdateText();
    }

    public void TakeDamage(int amount, GameObject source)
    {
        if(totalCharge < maxChargePerShield)
        {
            TbMonsterAI monsterAI = source.GetComponent<TbMonsterAI>(); // In case I want to add damage from other sources
            if(monsterAI)
            {
                audioSource.PlayOneShot(unblockedAttackClip);
                monsterAI.UnblockedHit();
            }
            else
            {
                if(source == gameObject)
                {
                    //collector.SetCountdown(10);
                }
            }
        }
        audioSource.PlayOneShot(hitOnChargedShieldClip);
        totalCharge = Mathf.Clamp(totalCharge - amount, 0, 100);
        UpdateShields();
    }

    public void UpdateText()
    {
        if(chargingShieldIndex == shieldCount - 1 && chargingShield.value == 1)
        {
            chargeText.text = "MAX";
        }
        else
        {
            chargeText.text = (maxChargePerShield - chargeOnCurrentShield).ToString();
        }
    }

    private void UpdateSlider()
    {
        for(int i = 0; i < chargingShieldIndex; i++) // color newly charged shields
        {
            Slider updatingSlider = shieldSliderList[i];
            updatingSlider.value = 1;
            updatingSlider.transform.Find("Fill Area").transform.Find("Shield Fill").transform.GetComponent<Image>().color = shieldChargedColor;
        }
        for(int i = chargingShieldIndex + 1; i < shieldCount; i++) // color newly uncharged shields
        {
            shieldSliderList[i].value = 0;
            Image fill = chargingShield.transform.Find("Fill Area").transform.Find("Shield Fill").transform.GetComponent<Image>();
            fill.color = shieldUnchargedColor;
        }
        chargingShield.value = chargeOnCurrentShield / (float)maxChargePerShield;
        Image shieldFill = chargingShield.transform.Find("Fill Area").transform.Find("Shield Fill").transform.GetComponent<Image>();
        if(chargingShield.value == 1)
        {
            shieldFill.color = shieldChargedColor;
        }
        else
        {
            shieldFill.color = shieldUnchargedColor;
        }
    }

    public int GetMaxChargePerShield()
    {
        return maxChargePerShield;
    }
}
