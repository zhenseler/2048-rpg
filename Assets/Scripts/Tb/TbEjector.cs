﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TbEjector : MonoBehaviour
{
    // Config params
    [SerializeField] List<TbCollector> collectorList = new List<TbCollector>();
    [SerializeField] AudioClip ejectClip;

    KeyCode keycode;
    AudioSource audioSource;

    private void Start()
    {
        keycode = FindObjectOfType<KeyBindings>().ejectorKey;
        audioSource = GetComponent<AudioSource>();
    }

    private void Update() 
    {
        if(Input.GetKeyDown(keycode))
        {
            foreach(TbCollector collector in collectorList)
            {
                audioSource.PlayOneShot(ejectClip);
                collector.CollectBlock();
                collector.ResetCountdown();
            }
        }
    }
}
