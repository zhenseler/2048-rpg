﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class TbCollector : MonoBehaviour
{

    // Config params
    [SerializeField] int countdown;
    [SerializeField] Image cooldownImage;
    [SerializeField] bool canJam; // Whether the collector can jam by taking up monster blocks
    [SerializeField] Image jammedImage;
    [SerializeField] TextMeshProUGUI jammedText;


    // State vars
    int tileLayerMask = 1 << 8;
    TileSpawner tileSpawner;
    int currentCountdown;
    int jammedCounter;
    bool jammed = false;

    // Cached refs
    BoxCollider boxCollider;

    private void Start() 
    {
        tileSpawner = FindObjectOfType<TileSpawner>();
        tileSpawner.onTurnFinish += TrackCountdown;
        boxCollider = GetComponent<BoxCollider>();
        boxCollider.enabled = false;
    }



    public int CollectBlock()
    {
        if(currentCountdown == 0)
        {
            RaycastHit hit;
            if(Physics.Raycast(transform.position, Vector3.forward, out hit, Mathf.Infinity, tileLayerMask))
            {
                NumberTile hitTile = hit.transform.GetComponentInParent<NumberTile>();
                if(hitTile)
                {
                    int tileValue = hitTile.GetValue();
                    hitTile.RemoveTile();
                    if(tileValue < 0 && canJam) 
                    {
                        Jam(Mathf.Abs(tileValue)); 
                        ResetCountdown();
                        return 0;
                    }
                    //ResetCountdown();
                    return tileValue;
                }
            }
        }
        print("Null case happened");
        return 0; // in null case, return 0
    }


    private void TrackCountdown()
    {
        if(jammedCounter > 0)
        {
            DecreaseJam();
            if(jammedCounter == 0)
            {
                Unjam();
                return;
            }
            return;
        }
        if(currentCountdown > 0)
        {
            currentCountdown--;
            cooldownImage.fillAmount = Mathf.Clamp(currentCountdown / (float)countdown, 0, 1);
        }
        if(currentCountdown == 0 && boxCollider.enabled == true) 
        {
            boxCollider.enabled = false;
            tileSpawner.IncreaseAvailableSpaces(1);
        }
    }

    public bool Ready()
    {
        if(currentCountdown == 0) {return true;}
        else {return false;}
    }

    public void ResetCountdown()
    {
        currentCountdown = countdown;
        cooldownImage.fillAmount = Mathf.Clamp(currentCountdown / countdown, 0, 1);
        boxCollider.enabled = true;
        tileSpawner.DecreaseAvailableSpaces(1);
    }

    private void Jam(int turns)
    {
        jammed = true;
        jammedCounter = turns;
        jammedImage.enabled = true;
        jammedText.enabled = true;
        jammedText.text = jammedCounter.ToString();
    }

    private void DecreaseJam()
    {
        jammedCounter--;
        jammedText.text = jammedCounter.ToString();
    }

    private void Unjam()
    {
        jammed = false;
        jammedImage.enabled = false;
        jammedText.enabled = false;
    }


}
