﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TbCannon : MonoBehaviour
{
    // Config params
    [SerializeField] List<TbCollector> collectorList = new List<TbCollector>();

    // State vars
    int monsterLayerMask = 1 << 10;
    KeyCode keycode;
    TbMonsterAI tbMonsterAI;


    void Start()
    {
        keycode = FindObjectOfType<KeyBindings>().cannonKey;
        tbMonsterAI = FindObjectOfType<TbMonsterAI>();
    }

    void Update()
    {
        if(Input.GetKeyDown(keycode))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        foreach(TbCollector collector in collectorList)
        {
            int damage = collector.CollectBlock();
            tbMonsterAI.DecreaseHealth(damage);
            collector.ResetCountdown();
        }        
    }
}
